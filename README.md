## Husky
iniciar projeto
```
yarn init -y
```

adiconar o Husky como dependencia
```
yarn add -D husky
```
comando para preparar o husky
```
yarn husky install
```

adicionando no package.json para rodar automaticamente
```
{
    ...
    "scripts": {
        ...
        "postinstall": "husky install"
    }

}
```
testando os hooks
```
yarn husky set .husky/pre-commit "exit 1"
```
```
yarn husky set .husky/pre-commit "echo Teste1"
```
```
yarn husky add .husky/pre-commit "exit 1"
```


#### [Commitlint](https://commitlint.js.org/#/reference-rules)

Usado para verificar se a mensagem de commit está com o padrão definido

adicionando a dependencia
```
yarn add -D @commitlint/cli @commitlint/config-conventional
```
adicionando arquivo de configurações
```
echo "module.exports = {extends: ['@commitlint/config-conventional']}" > commitlint.config.js
```
configurando o hook
```
yarn husky set .husky/commit-msg "yarn commitlint --edit $1"
```


#### [lint-staged](https://github.com/okonet/lint-staged)
Usado neste projeto para verificar se o código está com a formatação padrão definida e também para verificar erros de sintaxe

adicionando as dependencias que realizam a verificação de formatação e sintaxe
```
yarn add -D eslint prettier eslint-plugin-import eslint-config-airbnb-base eslint-plugin-prettier eslint-config-prettier
```
adicionando a dependencia utilizada pelo hook
```
yarn add -D lint-staged
```
adicionando as configurações
```
echo "{ \"*.{ts,js}\": [\"eslint --fix src --ext .js,.ts\", \"prettier --write\"] }" > .lintstagedrc.json
```
configurando o hook
```
yarn husky set .husky/pre-commit "yarn lint-staged"
```

#### [commitizen](https://commitizen-tools.github.io/commitizen/)
adicionando a dependencia
```
yarn add -D commitizen cz-conventional-changelog
```
configuração no package.json
```
{
    ...
    "scripts": {
        ...
        "commit": "git-cz"
    },
    ...
    "config": {
        "commitizen": {
            "path": "./node_modules/cz-conventional-changelog"
        }
    }

}
```
utilizar cli
```
yarn commit
```

### opcionais

 - Substituir "git commit" pela chamada do commitizen
```
yarn husky set .husky/prepare-commit-msg "exec < /dev/tty && node_modules/.bin/cz --hook || true"
```


 - Adicionar script de teste
```
yarn husky set .husky/pre-push "yarn test"
```

[Git Hooks Doc](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks)